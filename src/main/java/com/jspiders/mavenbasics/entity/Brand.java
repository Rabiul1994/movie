package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.BRAND_INFO)
public class Brand implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="brand_id")
	private Long id;
	
	@Column(name="brand_name")
	private String brandName;
	
	@Column(name="headquater")
	private String headquater;
	
	public Brand() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getHeadquater() {
		return headquater;
	}

	public void setHeadquater(String headquater) {
		this.headquater = headquater;
	}
	
	
}
