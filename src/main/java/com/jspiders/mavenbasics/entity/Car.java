package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.CAR_INFO)
public class Car implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="car_id")
	private Long id;
	
	@Column(name="car_name")
	private String name;
	
	@Column(name="fuel_type")
	private String fuelType;
	
	@Column(name="seats")
	private String numberOfSeats;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="brand_id")
	private Brand brand;
	
	public Car() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(String numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	
	
}
