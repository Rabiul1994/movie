package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.COUNTRY_INFO)
public class Country implements Serializable
{
	@Id
	@GenericGenerator(name="c_auto", strategy="increment")
	@GeneratedValue(generator="c_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="population")
	private Long population;
	
	@Column(name="capital")
	private String capital;
	
	@Column(name="area")
	private String area;
	
	@Column(name="continent")
	private String continent;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="fro_key")
	private PrimeMinister primeMinister;
	
	public Country() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PrimeMinister getPrimeMinister() {
		return primeMinister;
	}

	public void setPrimeMinister(PrimeMinister primeMinister) {
		this.primeMinister = primeMinister;
	}
	
	
}
