package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.USER_INFO)
public class CarUser implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="u_id")
	private Long id;
	
	@Column(name="user_name")
	private String userName;
	
	@ManyToMany(cascade=CascadeType.ALL, mappedBy="user")
	private List<Vehicle> vehicle;
	
	public CarUser() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Vehicle> getVehicle() {
		return vehicle;
	}

	public void setVehicle(List<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}


	
	
}
