package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.DEPARTMENT_INFO)
public class Department implements Serializable
{
	@Id
	@GenericGenerator(name = "d_auto", strategy = "increment")
	@GeneratedValue(generator="d_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="manager_name")
	private String managerName;
	
	@Column(name="number_of_employees")
	private String numberOfEmployees;
	
	public Department() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(String numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}
	
	
}
