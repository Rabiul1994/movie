package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name = AppConstants.MOVIE_INFO)
public class Movie implements Serializable
{
	@Id      //primary key
	@GenericGenerator(name="m_auto", strategy="increment")
	@GeneratedValue(generator="m_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="rating")
	private String rating;
	
	@Column(name="budget")
	private Double buget;
	
	@Column(name="release_date")
	private Date releaseDate;
	
	public Movie() {}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getRating() {
		return rating;
	}
	
	public void setRating(String rating) {
		this.rating = rating;
	}

	public Double getBuget() {
		return buget;
	}
	
	public void setBuget(Double buget) {
		this.buget = buget;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}
	
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", name=" + name + ", rating=" + rating + ", buget=" + buget + ", releaseDate="
				+ releaseDate + "]";
	}
	
}
