package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name="EMPLOYEES")
public class Employee implements Serializable {
 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="EMP_ID", unique = true, nullable = false)
    private Long empId;
 
    private String name;
         
    @OneToMany(mappedBy="employee", cascade = CascadeType.ALL)
    private List<AssetMgnt> assetMgnt;
 
    public List<AssetMgnt> getAssetMgnt() {
        return assetMgnt;
    }
 
    public void setAssetMgnt(List<AssetMgnt> assetMgnt) {
        this.assetMgnt = assetMgnt;
    }
 
    public Long getEmpId() {
        return empId;
    }
 
    public void setEmpId(Long empId) {
        this.empId = empId;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    @Override
    public String toString() {
 
        String resp = this.empId+" | "+this.name+" | ";
 
        return resp;
    }
}