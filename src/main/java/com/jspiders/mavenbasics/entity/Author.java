package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.AUTHOR_INFO)
public class Author implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="a_id")
	private Long id;
	
	@Column(name="author_name")
	private String authorName;
	
	public Author() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	
}
