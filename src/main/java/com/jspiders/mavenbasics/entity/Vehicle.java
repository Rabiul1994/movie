package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.VEHICLE_INFO)
public class Vehicle implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="v_id")
	private Long id;
	
	@Column(name="v_name")
	private String vehicleName;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="vehicle_user", joinColumns=@JoinColumn(name="vehicle_id"), inverseJoinColumns=@JoinColumn(name="user_id"))
	private List<CarUser> user;
	
	public Vehicle() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public List<CarUser> getUser() {
		return user;
	}

	public void setUser(List<CarUser> user) {
		this.user = user;
	}


	
	
}
