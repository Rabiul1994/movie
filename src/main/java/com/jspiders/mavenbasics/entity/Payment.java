package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;


@Entity @Table(name=AppConstants.PAYMENT_INFO)
public class Payment implements Serializable
{
	@Id
	@GenericGenerator(name="m_auto", strategy="increment")
	@GeneratedValue(generator="m_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="movie_id")
	private Long movieId;
	
	@Column(name="number_of_tickets")
	private Long numberOfTickets;
	
	@Column(name="price")
	private Double price;
	
	@Column(name="show_date")
	private Date showDate;
	
	@Column(name="show_time")
	private String showTime;
	
	@Column(name="payment_type")
	private String paymentType;
	
	@Column(name="total_price")
	private Double totalPrice;
	
	public Payment() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}

	public Long getNumberOfTickets() {
		return numberOfTickets;
	}

	public void setNumberOfTickets(Long numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getShowDate() {
		return showDate;
	}

	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}

	public String getShowTime() {
		return showTime;
	}

	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", movieId=" + movieId + ", numberOfTickets=" + numberOfTickets + ", price="
				+ price + ", showDate=" + showDate + ", showTime=" + showTime + ", paymentType=" + paymentType
				+ ", totalPrice=" + totalPrice + "]";
	}
	
	
	
}
