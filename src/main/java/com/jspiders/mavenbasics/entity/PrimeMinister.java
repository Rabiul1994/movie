package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name=AppConstants.PM_INFO)
public class PrimeMinister implements Serializable
{
	@Id
	@GenericGenerator(name="p_auto", strategy="increment")
	@GeneratedValue(generator="p_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="age")
	private Long age;
	
	@Column(name="party_name")
	private String partyName;
	
	public PrimeMinister() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
}
