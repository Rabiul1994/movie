package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
 
@Entity
@Table(name="ASSET_MNGT")
public class AssetMgnt implements Serializable{
 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="AM_ID", unique = true, nullable = false)
    private Long amId;
 
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="EMP_ID")
    private Employee employee;
 
    @Column(name="ASSET_NAME")
    private String assetName;
 
    public Long getAmId() {
        return amId;
    }
 
    public void setAmId(Long amId) {
        this.amId = amId;
    }
 
    public Employee getEmployee() {
        return employee;
    }
 
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
 
    public String getAssetName() {
        return assetName;
    }
 
    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }
 
    public String toString() {
 
        return this.assetName+" | ";
    }
}
