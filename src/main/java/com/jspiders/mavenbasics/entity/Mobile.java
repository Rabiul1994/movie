package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;


@Entity
@Table(name=AppConstants.MOBILE_INFO)
public class Mobile implements Serializable
{
	@Id
	@GenericGenerator(name = "a_auto", strategy = "increment")
	@GeneratedValue(generator="a_auto")
	@Column(name="m_id")
	private Long id;
	
	@Column(name="mobile_name")
	private String mobileName;
	
	@Column(name="launch_year")
	private String launchYear;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="mobile")
	private List<Application> applist;
	
	
	
	public Mobile() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobileName() {
		return mobileName;
	}

	public void setMobileName(String mobileName) {
		this.mobileName = mobileName;
	}

	public String getLaunchYear() {
		return launchYear;
	}

	public void setLaunchYear(String launchYear) {
		this.launchYear = launchYear;
	}

	public List<Application> getApplist() {
		return applist;
	}

	public void setApplist(List<Application> applist) {
		this.applist = applist;
	}
}
