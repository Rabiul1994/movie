package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity @Table(name= AppConstants.COMPANY_INFO)
public class Company implements Serializable
{
	@Id
	@GenericGenerator(name = "c_auto", strategy = "increment")
	@GeneratedValue(generator="c_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="ceo_name")
	private String ceoName;
	
	@Column(name="headquarters")
	private String headquarters;
	
	@Column(name="total_employee")
	private String totalEmployee;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="c_id")
	private List<Department> departmentList;
	
	public Company() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCeoName() {
		return ceoName;
	}

	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}

	public String getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}

	public String getTotalEmployee() {
		return totalEmployee;
	}

	public void setTotalEmployee(String totalEmployee) {
		this.totalEmployee = totalEmployee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}
	
	
}
