package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity
@Table(name=AppConstants.APPLICATION_INFO)
public class Application implements Serializable
{
	@Id
	@GenericGenerator(name = "a_auto", strategy = "increment")
	@GeneratedValue(generator="a_auto")
	@Column(name="a_id")
	private Long id;
	
	@Column(name="app_name")
	private String appName;
	
	@Column(name="app_type")
	private String appType;
	
	@Column(name="app_ceo")
	private String appCeo;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="m_id")
	private Mobile mobile;
	
	public Application() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppCeo() {
		return appCeo;
	}

	public void setAppCeo(String appCeo) {
		this.appCeo = appCeo;
	}

	public Mobile getMobile() {
		return mobile;
	}

	public void setMobile(Mobile mobile) {
		this.mobile = mobile;
	}

	
}
