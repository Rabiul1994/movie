package com.jspiders.mavenbasics.constants;

public interface AppConstants {
	
	public static String MOVIE_INFO="movie_info";
	public static String PAYMENT_INFO="payment_info";
	public static String COUNTRY_INFO="country_info";
	public static String PM_INFO="pm_info";
	public static String COMPANY_INFO="company_info";
	public static String DEPARTMENT_INFO="department_info";
	public static String APPLICATION_INFO="applications_info";
	public static String MOBILE_INFO="mobile_info";
	public static String CAR_INFO="car_info";
	public static String BRAND_INFO="brand_details";
	public static String AUTHOR_INFO="author_info";
	public static String BOOK_INFO="book_info";
	public static String USER_INFO="user_info";
	public static String VEHICLE_INFO="vehicle_info";
	
}
