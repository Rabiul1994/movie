package com.jspiders.mavenbasics.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.mavenbasics.entity.Payment;
import com.jspiders.mavenbasicsSfUtil.SessionFactoryUtil;

public class PaymentRepository 
{
	public void savePaymentDetails(Payment payment)
	{
		SessionFactory sf = SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		payment.setTotalPrice(payment.getNumberOfTickets()*payment.getPrice());
		session.save(payment);
		transaction.commit();
		sf.close();
	}
}
