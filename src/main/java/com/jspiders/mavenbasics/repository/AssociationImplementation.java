package com.jspiders.mavenbasics.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.mavenbasics.entity.Application;
import com.jspiders.mavenbasics.entity.Book;
import com.jspiders.mavenbasics.entity.Car;
import com.jspiders.mavenbasics.entity.Company;
import com.jspiders.mavenbasics.entity.Country;
import com.jspiders.mavenbasics.entity.Employee;
import com.jspiders.mavenbasics.entity.Mobile;
import com.jspiders.mavenbasics.entity.Vehicle;
import com.jspiders.mavenbasicsSfUtil.SessionFactoryUtil;

public class AssociationImplementation implements Association {

	@Override
	public void saveCountryDetails(Country country) {
		
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(country);
		transaction.commit();
		sessionFactory.close();
	}

	@Override
	public void saveCompanyDetails(Company company) {
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(company);
		transaction.commit();
		sessionFactory.close();
		
	}

	@Override
	public void saveMobileDetails(Mobile mobile) {
		
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(mobile);
		transaction.commit();
		sessionFactory.close();
		
	}

	@Override
	public void saveEmployeeDetails(Employee employee) {
		
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		sessionFactory.close();
	}

	@Override
	public void saveCarDetails(Car car) {
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(car);
		transaction.commit();
//		sessionFactory.close();
		
	}

	@Override
	public void saveBookDetails(Book book) {
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(book);
		transaction.commit();
		
	}

	@Override
	public void saveVehicleDetails(Vehicle vehicle) {
		
		SessionFactory sessionFactory = SessionFactoryUtil.getInstance();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(vehicle);
		transaction.commit();
	}

}
