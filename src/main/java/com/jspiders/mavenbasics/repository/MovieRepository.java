package com.jspiders.mavenbasics.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.jspiders.mavenbasics.entity.Movie;
import com.jspiders.mavenbasicsSfUtil.SessionFactoryUtil;

public class MovieRepository 
{
	public void saveMovieDetails(Movie movie)
	{
		try
		{
			SessionFactory sf=SessionFactoryUtil.getInstance();
			Session session = sf.openSession();
			Transaction transaction= session.beginTransaction();
			session.save(movie);
			transaction.commit();
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
	}
	
	public Movie getMovieId(Long id)
	{
		Movie m=null;
		try {
			SessionFactory sf=SessionFactoryUtil.getInstance();
			Session session = sf.openSession();
			m=session.get(Movie.class, id);
		}
		catch (HibernateException e) {
			// TODO: handle exception
		}
		return m;
		
	}
	
	public void updateRatingAndBudgetById(String rating, Double budget, Long id)
	{
		Movie m= getMovieId(id);
		if(m!=null)
		{
			m.setRating(rating);
			m.setBuget(budget);
		}
		SessionFactory sf=SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		Transaction transaction= session.beginTransaction();
		session.saveOrUpdate(m);
		transaction.commit();
		
	}
	
	public List<Movie> findAll()
	{
		Configuration cfg = new Configuration();
		SessionFactory sf=SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		String hql="from Movie";
		Query query = session.createQuery(hql);
		return query.list();
	}
	
	public Movie findByName(String movieName)
	{
		SessionFactory sf=SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		String hql="from Movie where name=:mvName";  // =: for dynamic i/p
		Query query = session.createQuery(hql);
		query.setParameter("mvName", movieName); //sets the value and mapped with method argument
		return (Movie) query.uniqueResult();  // use this if you have unique mv names.. if not use list()
		
	}
	
	public void updateRatingAndBudgetByName(String name, String rating, Double buget)
	{
		SessionFactory sf=SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update Movie set rating=:r, budget=:b where name=:n";
		Query query = session.createQuery(hql);
		query.setParameter("r", rating);
		query.setParameter("b", buget);
		query.setParameter("n", name);
		
		int rowsUpdate = query.executeUpdate();
		transaction.commit();
		
		if(rowsUpdate>0) System.out.println("Update success");
		else System.out.println("Update failed");
		
	}
	
	public void deleteRowByName(String name)
	{
		SessionFactory sf=SessionFactoryUtil.getInstance();
		Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete Movie where name=:mn";
		Query query = session.createQuery(hql);
		
		query.setParameter("mn", name);
		int rowaffected = query.executeUpdate();
		transaction.commit();
		
		if(rowaffected>0) System.out.println("Delete Successfull");
		else System.out.println("Delete failed");
	}
	
	
	
	
	
	
	
	
	
	
	
}
