package com.jspiders.mavenbasics.repository;

import com.jspiders.mavenbasics.entity.Book;
import com.jspiders.mavenbasics.entity.Car;
import com.jspiders.mavenbasics.entity.Company;
import com.jspiders.mavenbasics.entity.Country;
import com.jspiders.mavenbasics.entity.Employee;
import com.jspiders.mavenbasics.entity.Mobile;
import com.jspiders.mavenbasics.entity.Vehicle;

public interface Association {
	
	public void saveCountryDetails(Country country);
	public void saveCompanyDetails(Company company);
	public void saveMobileDetails(Mobile mobile);
	public void saveEmployeeDetails(Employee employee);
	public void saveCarDetails(Car car);
	public void saveBookDetails(Book book);
	public void saveVehicleDetails(Vehicle vehicle);
	
	


}
