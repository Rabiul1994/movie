package com.jspiders.mavenbasics;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jspiders.mavenbasics.entity.Application;
import com.jspiders.mavenbasics.entity.AssetMgnt;
import com.jspiders.mavenbasics.entity.Author;
import com.jspiders.mavenbasics.entity.Book;
import com.jspiders.mavenbasics.entity.Brand;
import com.jspiders.mavenbasics.entity.Car;
import com.jspiders.mavenbasics.entity.CarUser;
import com.jspiders.mavenbasics.entity.Company;
import com.jspiders.mavenbasics.entity.Country;
import com.jspiders.mavenbasics.entity.Department;
import com.jspiders.mavenbasics.entity.Employee;
import com.jspiders.mavenbasics.entity.Mobile;
import com.jspiders.mavenbasics.entity.Movie;
import com.jspiders.mavenbasics.entity.Payment;
import com.jspiders.mavenbasics.entity.PrimeMinister;
import com.jspiders.mavenbasics.entity.Vehicle;
import com.jspiders.mavenbasics.repository.AssociationImplementation;
import com.jspiders.mavenbasics.repository.MovieRepository;
import com.jspiders.mavenbasics.repository.PaymentRepository;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
       Movie movie=new Movie();
//       movie.setId(124L);
       movie.setName("Beast");
       movie.setBuget(100D);
       movie.setRating("4");
       movie.setReleaseDate(new Date());
//       
//       MovieRepository movieRepository = new MovieRepository();
//       movieRepository.saveMovieDetails(movie);
//      movieRepository.updateRatingAndBudgetById("4.5", 400D, 124L);
       
//      System.out.println(movieRepository.getMovieId(124L));
       
//       List<Movie> movieDetails = movieRepository.findAll();
//       
//       movieDetails.forEach(mv -> {    // forEach lamda
//       System.out.println(mv);
//       });
       
//       System.out.println(movieRepository.findByName("RRR"));
       
//       movieRepository.updateRatingAndBudgetByName("Beast", "3.5", 200D);
       
//       movieRepository.deleteRowByName("Mission Impossible");
    	
//    	Payment payment = new Payment();
//    	payment.setMovieId(movie.getId());
//    	payment.setNumberOfTickets(70L);
//    	payment.setPrice(50D);
//    	payment.setShowDate(new Date());
//    	payment.setShowTime("5:00 PM");
//    	payment.setPaymentType("Cash");
//    	
//    	PaymentRepository paymentRepository = new PaymentRepository();
//    	paymentRepository.savePaymentDetails(payment);
    	
    	
    	Country country = new Country();
    	country.setName("People's Republic of China");
    	country.setCapital("Beijing");
    	country.setPopulation(1420000000L);
    	country.setArea("9.6 million sq km");
    	country.setContinent("Asia");
    	
    	PrimeMinister pm = new PrimeMinister();
    	
    	country.setPrimeMinister(pm);
    	
    	pm.setName("Xi Jinping");
    	pm.setAge(69L);
    	pm.setGender("Male");
    	pm.setPartyName("Chinese Communist Party");
    	
    	AssociationImplementation associationImplementation = new AssociationImplementation();
//    	associationImplementation.saveCountryDetails(country);
    	
    	Department dept1 = new Department();
    	dept1.setName("HR");
    	dept1.setManagerName("Rincy");
    	dept1.setNumberOfEmployees("30");
    	
    	Department dept2 = new Department();
    	dept2.setName("Sales");
    	dept2.setManagerName("RamuKaka");
    	dept2.setNumberOfEmployees("35");
    	
    	Department dept3 = new Department();
    	dept3.setName("Quality");
    	dept3.setManagerName("Naruttam");
    	dept3.setNumberOfEmployees("25");
    	
    	List<Department> deptList = new ArrayList<Department>();
    	
    	deptList.add(dept1);
    	deptList.add(dept2);
    	deptList.add(dept3);
    	
    	Company company = new Company();
    	company.setName("Micro-Services");
    	company.setCeoName("Rabiul");
    	company.setHeadquarters("Bangalore");
    	company.setTotalEmployee("150");
    	
    	company.setDepartmentList(deptList);
    	
//    	associationImplementation.saveCompanyDetails(company);
    	
    	Mobile mobile = new Mobile();
    	mobile.setMobileName("OnePlus Pro 10");
    	mobile.setLaunchYear("2022");
    	
    	Application app1 = new Application();
    	app1.setAppName("Flipkart");
    	app1.setAppType("Shopping");
    	app1.setAppCeo("Kalyan KrishnaMurty");
    	app1.setMobile(mobile);
    	
    	Application app2 = new Application();
    	app2.setAppName("PharmEasy");
    	app2.setAppType("Medicine");
    	app2.setAppCeo("Siddhart Shah");
    	app2.setMobile(mobile);
    	
    	Application app3 = new Application();
    	app3.setAppName("Whatsapp India");
    	app3.setAppType("Messaging");
    	app3.setAppCeo("Abhijit Bose");
    	app3.setMobile(mobile);
    	
    	List<Application> appList = new ArrayList<Application>();
    	appList.add(app1);
    	appList.add(app2);
    	appList.add(app3);
    	
    	
    	mobile.setApplist(appList);
    	
    	
//    	associationImplementation.saveMobileDetails(mobile);
    	
    	Employee emp1=new Employee();
    	emp1.setName("raj");
    	
    	
    	AssetMgnt mgmt1=new AssetMgnt();
    	mgmt1.setAssetName("radio");
    	mgmt1.setEmployee(emp1);
    	AssetMgnt mgmt2=new AssetMgnt();
    	mgmt2.setAssetName("watch");
    	mgmt2.setEmployee(emp1);
    	
    	List<AssetMgnt> list=new ArrayList<>();
    	list.add(mgmt1);
    	list.add(mgmt2);
    	emp1.setAssetMgnt(list);
    	
//    	associationImplementation.saveEmployeeDetails(emp1);
    	
    	Brand brand = new Brand();
    	brand.setBrandName("Tata Motors");
    	brand.setHeadquater("Mumbai");
    	
    	Car c1 = new Car();
    	c1.setName("Harrier");
    	c1.setFuelType("Diesel");
    	c1.setNumberOfSeats("4");
    	c1.setBrand(brand);
    	
    	Car c2 = new Car();
    	c2.setName("Nexon");
    	c2.setFuelType("Petrol");
    	c2.setNumberOfSeats("6");
    	c2.setBrand(brand);
    	
    	
//    	associationImplementation.saveCarDetails(c1);
//    	associationImplementation.saveCarDetails(c2);
    	
    	Author author1 = new Author();
    	author1.setAuthorName("Lisa Lutz");
    	
    	Author author2 = new Author();
    	author2.setAuthorName("David Hayward");
    	
    	Author author3 = new Author();
    	author3.setAuthorName("William Shakesphere");
    	
    	Set<Author> authorSet = new HashSet<>();
    	authorSet.add(author1);
    	authorSet.add(author2);
    	
    	Book book = new Book();
    	book.setBookName("Heads You Lose");
    	book.setAuthors(authorSet);
    	
    	Set<Author> authorSet1 = new HashSet<>();
    	authorSet1.add(author3);
    	
    	Book book1 = new Book();
    	book1.setBookName("Julius Ceaser");
    	book1.setAuthors(authorSet1);
    	
//    	associationImplementation.saveBookDetails(book);
//    	associationImplementation.saveBookDetails(book1);
    	
    	CarUser user1 = new CarUser();
    	user1.setUserName("Rabiul");
    	
    	CarUser user2 = new CarUser();
    	user2.setUserName("Saddam");
    	
    	CarUser user3 = new CarUser();
    	user3.setUserName("Mustu");
    	
    	
    	
    	Vehicle vehicle1 = new Vehicle();
    	vehicle1.setVehicleName("Car");
    	
    	Vehicle vehicle2 = new Vehicle();
    	vehicle2.setVehicleName("Jeep");
    	
    	Vehicle vehicle3 = new Vehicle();
    	vehicle3.setVehicleName("Truck");
    	
    	
    	
    	
    	ArrayList<CarUser> userList = new ArrayList<>();
    	userList.add(user1);
    	userList.add(user2);
    	userList.add(user3);
    	
    	vehicle1.setUser(userList);
    	vehicle2.setUser(userList);
    	vehicle3.setUser(userList);
    	
    	ArrayList<Vehicle> vehicleList = new ArrayList<>();
    	
    	vehicleList.add(vehicle1);
    	vehicleList.add(vehicle2);
    	vehicleList.add(vehicle3);
    	for(int i=0;i<vehicleList.size();i++) {
    	associationImplementation.saveVehicleDetails(vehicleList.get(i));    	
    	}
    	
    	
    	
    	
    	
    }
}
