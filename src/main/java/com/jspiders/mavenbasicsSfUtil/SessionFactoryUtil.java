package com.jspiders.mavenbasicsSfUtil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil
{
	private static SessionFactory s=null;
	
	
	private SessionFactoryUtil() { }
	
	public static SessionFactory getInstance()
	{
		if(s==null) {
		Configuration cfg = new Configuration();
		cfg.configure();
		s=cfg.buildSessionFactory();
	}
		return s;
	
	}
}
